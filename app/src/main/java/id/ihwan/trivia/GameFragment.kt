package id.ihwan.trivia


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import id.ihwan.trivia.databinding.FragmentGameBinding
import id.ihwan.trivia.game.GameViewModel

class GameFragment : Fragment() {

   val viewModel: GameViewModel by lazy {
       ViewModelProviders.of(this).get(GameViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = DataBindingUtil.inflate<FragmentGameBinding>(
            inflater, R.layout.fragment_game, container, false)

        binding.submitButton.setOnClickListener{
            val checkedId = binding.questionRadioGroup.checkedRadioButtonId

            // Do nothing if nothing is checked (id == -1)
            if (-1 != checkedId) {
                var answerIndex = 0
                when (checkedId) {
                    R.id.secondAnswerRadioButton -> answerIndex = 1
                    R.id.thirdAnswerRadioButton -> answerIndex = 2
                    R.id.fourthAnswerRadioButton -> answerIndex = 3
                }
                // The first answer in the original question is always the correct one, so if our
                // answer matches, we have the correct answer.
                if (viewModel.answers[answerIndex] == viewModel.currentQuestion.answers[0]) {
                    viewModel.questionIndex++
                    // Advance to the next question
                    if (viewModel.questionIndex < viewModel.numQuestions) {
                        viewModel.currentQuestion = viewModel.questions[viewModel.questionIndex]
                        viewModel.setQuestion()
                        binding.invalidateAll()
                    } else {
                        it.findNavController().navigate(R.id.action_gameFragment_to_gameWonFragment)
                    }
                } else {
                    it.findNavController().navigate(R.id.action_gameFragment_to_gameOverFragment)                    // Game over! A wrong answer sends us to the gameOverFragment.
                }
            }
        }
        return binding.root
    }


}